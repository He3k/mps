##########
# TASK 1 #
##########

#include <avr/io.h>  // Include AVR IO library

void uart_init() {
    // Настройка скорости передачи (28800 бит/с)
    UBRR0 = 34;  // Рассчитанное значение UBRRn для f_osc = 16 МГц
    UCSR0A = 0;
    UCSR0B = (1 << TXEN0) | (1 << RXEN0);  // Включение передатчика и приемника
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);  // Размер данных: 8 бит, 1 стоп-бит, без бита четности
}

char uart_receive() {
    while (!(UCSR0A & (1 << RXC0)));  // Ждем, пока данные не будут готовы для чтения
    return UDR0;
}

void uart_transmit(char data) {
    while (!(UCSR0A & (1 << UDRE0)));  // Ждем, буфер
    UDR0 = data;  // Загрузка данных в буфер
}

int main(void) {
    // Инициализация UART
    uart_init();

    char surname[] = "\nSidorov";

    // Transmit the surname via UART
    for (int i = 0; i < sizeof(surname) - 1; i++) {
        uart_transmit(surname[i]);
      	_delay_ms(100); // Добавленная задержка между символами
    }

    while (1) {
        char input = uart_receive();
        uart_transmit(input);
    }

    return 0;
}


##########
# TASK 2 #
##########

#include <avr/io.h>

void uart_init() {
    // Calculate UBRR value for a baud rate of 28800
    UBRR0 = 34;  // Рассчитанное значение UBRRn для f_osc = 16 МГц
    UCSR0A = 0;
    UCSR0B = (1 << TXEN0) | (1 << RXEN0);  // Включение передатчика и приемника
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);  // Размер данных: 8 бит, 1 стоп-бит, без бита четности
}

char uart_receive() {
    while (!(UCSR0A & (1 << RXC0)));  // Ждем, пока данные не будут готовы для чтения
    return UDR0;  // Возвращаем данные
}

void uart_transmit(char data) {
    while (!(UCSR0A & (1 << UDRE0)));  // Ждем буфер
    UDR0 = data;  // Загружаем буфер
}

int main(void) {
    // Инициализация UART
    uart_init();

    char startSignal;
    while (1) {
        startSignal = uart_receive();  // Ждем сигнала
        if (startSignal == 'A') {  // Ожидание А
            break;  // Прерывание loop и начало передаачи
        }
    }

    char surname[] = "\nSidorov";

    // Передача фамилии через UART
    for (int i = 0; i < sizeof(surname) - 1; i++) {
        uart_transmit(surname[i]);
      	_delay_ms(100); // Добавленная задержка между символами
    }

    while (1) {
        char input = uart_receive();
        uart_transmit(input);
    }

    return 0;
}
