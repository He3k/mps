#define BIT0 2
#define BIT1 3
#define BIT2 4
#define BIT3 5
#define SIZE_OF_SINE_TABLE 1024  // Увеличение размера таблицы для более плавного сигнала

// Таблица значений синуса
byte sineTable[SIZE_OF_SINE_TABLE];

void setup() {
  Serial.begin(9600);
  pinMode(BIT0, OUTPUT);
  pinMode(BIT1, OUTPUT);
  pinMode(BIT2, OUTPUT);
  pinMode(BIT3, OUTPUT);

  // Заполнение таблицы значений синуса
  for (int i = 0; i < SIZE_OF_SINE_TABLE; i++) {
    float radians = (i * 2.0 * PI) / SIZE_OF_SINE_TABLE; // Преобразуем индекс в радианы
    // Нормализуем и масштабируем синус от 0 до 15 (для 4-битного значения)
    sineTable[i] = (sin(radians) * 7.5 + 7.5);
  }
}

void loop() {
  // Вывод синусоиды
  for (int i = 0; i < SIZE_OF_SINE_TABLE; i++) {
    setR2RDAC(sineTable[i]);
    //Serial.println(sineTable[i]);
    delayMicroseconds(25); // Уменьшение задержки для более быстрого обновления значений
  }
}

void setR2RDAC(byte value) {
  digitalWrite(BIT0, value & 1);
  digitalWrite(BIT1, value & 2);
  digitalWrite(BIT2, value & 4);
  digitalWrite(BIT3, value & 8);
}
