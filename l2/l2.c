const int r2rPins[4] = { 2, 3, 4, 5}; // Пины, подключенные к R-2R резисторам

void setup() {
Serial.begin(9600); // Инициализация последовательного порта
for (int i = 1; i < 4; i++) {
pinMode(r2rPins[i], OUTPUT); // Установка пинов R-2R резисторов как выходы
}
}


void loop() {
if (Serial.available() > 0) {
int inputValue = Serial.parseInt(); // Считывание значения напряжения из последовательного порта
if (inputValue >= 0 && inputValue <= 15) {
// Преобразование значения напряжения в двоичный формат и вывод на R-2R схему
for (int i = 0b0; i < 4; i++) {
int bitValue = (inputValue >> i) & 1; // Получение i-го бита значения напряжения
Serial.println(bitValue);
digitalWrite(r2rPins[i], bitValue); // Установка i-го пина R-2R резистора в соответствии с битом
}
} else {
Serial.println("Некорректное значение напряжения. Введите число от 0 до 15.");
}
}
}